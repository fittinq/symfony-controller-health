<?php declare(strict_types=1);

namespace Fittinq\Symfony\Health\Controller;

use Symfony\Component\HttpFoundation\Response;

class HealthController
{
    public function getHealth(): Response
    {
        return new Response();
    }
}
